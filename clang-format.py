#
# clang-format.py
#
# Copyright 2021 Tomi Lähteenmäki
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import gi

from gi.repository import GObject
from gi.repository import Gio
from gi.repository import Ide

class ClangFormatAddin(GObject.Object, Ide.BufferAddin):
    def do_load(self, buffer: Ide.Buffer):
        pass

    @staticmethod
    def get_git_root(file: Gio.File):
        launcher = Ide.SubprocessLauncher.new(0)
        launcher.set_flags(Gio.SubprocessFlags.STDOUT_PIPE)
        launcher.push_argv('git')
        launcher.push_argv('rev-parse')
        launcher.push_argv('--show-toplevel')
        launcher.set_run_on_host(True)

        work_dir = file.get_parent()
        launcher.set_cwd(work_dir.get_path())

        subprocess = launcher.spawn()
        try:
            ret, stdout, stderr = subprocess.communicate_utf8(None, None)
            if not ret:
                return None

            stdout = stdout.rstrip('\n')
            return stdout
        except Exception:
            return None

    @staticmethod
    def should_format(file: Gio.File):
        exts = ['.h', '.h.in',
                '.hpp', '.hpp.in',
                '.c', '.c.in',
                '.cc', '.cc.in',
                '.cpp', '.cpp.in',
                '.cxx', '.cxx.in']

        for ext in exts:
            if file.get_basename().endswith(ext):
                return True

        print('File {0} did not match any extension'.format(file.get_basename()))
        return False

    def do_save_file(self, buffer: Ide.Buffer, file: Gio.File):
        if not self.should_format(file):
            return

        work_dir = self.get_git_root(file)
        if work_dir is None:
            print('Failed to get Git root')
            return

        config_file = Gio.File.new_for_path('{0}/.clang-format'.format(work_dir))
        if not config_file.query_exists():
            print('No .clang-format file')
            return

        launcher = Ide.SubprocessLauncher.new(0)
        launcher.set_flags(Gio.SubprocessFlags.STDIN_PIPE |
                           Gio.SubprocessFlags.STDOUT_PIPE)
        launcher.push_argv('clang-format')
        launcher.set_run_on_host(True)
        launcher.set_cwd(work_dir)

        subprocess = launcher.spawn()

        begin, end = buffer.get_bounds()
        text = buffer.get_text(begin, end, True).replace('\r\n', '\n')
        ret, stdout, stderr = subprocess.communicate_utf8(text, None)
        if not ret:
            print("clang-format error:", stderr)
        else:
            buffer.begin_user_action()
            buffer.set_text(stdout.replace('\n', '\r\n'))
            buffer.end_user_action()

    def do_unload(self, page: Ide.Buffer):
        pass

